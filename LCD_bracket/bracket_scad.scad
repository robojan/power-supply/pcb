thickness = 2;
$fn=64;

module front() {
    color("blue") translate([-10,-6,-2]){
        difference() {
            cube([147,96,2]);
            translate([7,3,-5])
            {
                cube([90,62,10]);
            }
        }
    }
}

module lcd() {
    color("gray"){
        translate([0,0,-3.5]) {
            cube(size=[84,55,3.5]);
        }
    }   
}

module lcdScreen(height=0.1)
{
    translate([1, 1, -0.1]) {
        color("red") {
            cube(size=[76,51.5,height+0.1]);
        }
    }
}

module lcdClip(w=3)
{
    union() {
        cube([w,1.5,3.55]);
        translate([0,0,3.55])
        {
            polyhedron(points=[
                    [0,0,0], // 0
                    [w,0,0], // 1
                    [w,3,0], // 2
                    [0,3,0], // 3
                    [0,0,3], // 4
                    [w,0,3], // 5
                    [w,3,0.5], // 6
                    [0,3,0.5], // 7
                ], faces=[
                    [0,1,2,3],
                    [1,0,4,5],
                    [2,1,5,6],
                    [3,2,6,7],
                    [0,3,7,4],
                    [5,4,7,6]
                ]);
        }
    }
}

module fpClip(w=3)
{
    union() {
        cube([w,1.5,2.05]);
        translate([0,0,2.05])
        {
            polyhedron(points=[
                    [0,0,0], // 0
                    [w,0,0], // 1
                    [w,3,0], // 2
                    [0,3,0], // 3
                    [0,0,3], // 4
                    [w,0,3], // 5
                    [w,3,0.5], // 6
                    [0,3,0.5], // 7
                ], faces=[
                    [0,1,2,3],
                    [1,0,4,5],
                    [2,1,5,6],
                    [3,2,6,7],
                    [0,3,7,4],
                    [5,4,7,6]
                ]);
        }
    }
}

module pcbClip()
{
    union() {
        cube([2,10,11.6]);
        translate([2,0,8]) {
            cube([1, 10, 1]);
        }
        translate([2,0,10.6]) {
            cube([1, 10, 1]);
        }
    }
}

union(){
    difference() {
        hull() {
            cylinder(r=6, h=thickness);
            translate([84,0,0]) {
                cylinder(r=6, h=thickness);
            }
            translate([84,55,0]) {
                cylinder(r=6, h=thickness);
            }
            translate([0,55,0]) {
                cylinder(r=6, h=thickness);
            }
        }
        lcdScreen(10);
    }
    rotate([0,0,-90]) mirror([0,0,1]) translate([-42.5,-1.6,0])
    {
        lcdClip(w=30);
    }
    rotate([0,0,0]) mirror([0,0,1]) translate([10,-1.6,0])
    {        
        lcdClip(w=10);
    }
    rotate([0,0,0]) mirror([0,0,1]) translate([35,-1.6,0])
    {        
        lcdClip(w=10);
    }
    rotate([0,0,0]) mirror([0,0,1]) translate([60,-1.6,0])
    {        
        lcdClip(w=10);
    }
    rotate([0,0,180]) mirror([0,0,1]) translate([-70,-1.6-55,0])
    {        
        lcdClip(w=10);
    }
    rotate([0,0,180]) mirror([0,0,1]) translate([-45,-1.6-55,0])
    {        
        lcdClip(w=10);
    }
    rotate([0,0,180]) mirror([0,0,1]) translate([-20,-1.6-55,0])
    {        
        lcdClip(w=10);
    }
    translate([84,52,-3]) {
        cube([2,3,3]);
    }
    translate([84,0,-3]) {
        cube([2,3,3]);
    }
    rotate([0,0,0]) mirror([0,0,1]) translate([22.5,-1.6+59,0])
    {        
        fpClip(w=10);
    }
    rotate([0,0,0]) mirror([0,0,1]) translate([47.5,-1.6+59,0])
    {        
        fpClip(w=10);
    }
    rotate([0,0,180]) mirror([0,0,1]) translate([-32.5,-1.6+2.9,0])
    {        
        fpClip(w=10);
    }
    rotate([0,0,180]) mirror([0,0,1]) translate([-57.5,-1.6+2.9,0])
    {        
        fpClip(w=10);
    }
    rotate([0,0,90]) mirror([0,0,1]) translate([0,-1.6+2.9,0])
    {        
        fpClip(w=10);
    }
    rotate([0,0,90]) mirror([0,0,1]) translate([45,-1.6+2.9,0])
    {        
        fpClip(w=10);
    }
    mirror([0,0,1]) translate([85,0,0]){
        union() {
            pcbClip();
            translate([0, 45, 0]) {
                pcbClip();
            }
        }
    }
}

//lcd();
//front();